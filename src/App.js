import React, {Component} from 'react';
import './App.less';
import Home from "./page/home";

class App extends Component {
    render() {
        return (
            <div className='App'>
                <Home/>
            </div>
        );
    }
}

export default App;

